from django.test import TestCase, Client
from django.urls import reverse, resolve

# from .models import Matkul
from .views import *
from .urls import *

# Create your tests here.

class Story5TestCase(TestCase):

    def set_up(self):
        self.client = Client()
        pass

    def test_story5_url_status_200(self):
        # response = self.client.get('story5')
        # self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('story5:home'))
        self.assertEqual(response.status_code, 200)

    def test_story5_add_url_status_200(self):
        response = self.client.get('/story5/add/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('story5:add'))
        self.assertEqual(response.status_code, 200)

    def test_views_home(self):
        found = resolve('/story5/')            
        self.assertEqual(found.func, story5)
        pass

    def test_views_add(self):
        found = resolve('/story5/add/')            
        self.assertEqual(found.func, add)
        pass
    