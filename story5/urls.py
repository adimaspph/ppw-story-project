from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.story5, name='home'),
    path('add/', views.add, name='add'),
    path('<int:my_id>/', views.detail, name='detail'),
    path('<int:my_id>/delete/', views.delete_matkul, name='delete'),
]