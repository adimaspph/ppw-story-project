# Generated by Django 3.1.2 on 2020-10-16 02:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='sks',
            field=models.CharField(max_length=10),
        ),
        migrations.AlterField(
            model_name='matkul',
            name='tahun',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='post',
            name='content',
            field=models.CharField(max_length=500),
        ),
    ]
