from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=2)
    tahun = models.CharField(max_length=30)
    deskripsi = models.TextField(max_length=150)
    ruang = models.CharField(max_length=30)

class Post(models.Model):
    author = models.ForeignKey(Matkul, on_delete = models.CASCADE)
    content = models.CharField(max_length=500)
    published_date = models.DateTimeField(default=timezone.now)