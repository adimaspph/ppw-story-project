from django.shortcuts import render
from django.http import HttpResponseRedirect
from . import models
from .forms import Form_Matkul

# Create your views here.
def story5(request):
    matkuls = models.Matkul.objects.all()
    response = {
        "matkuls" : matkuls,
    }

    return render(request, 'story5/home.html', response)

def add(request):
    form = Form_Matkul(request.POST or None)
    response = {
        "form_matkul" : Form_Matkul,
    }
    
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/story5/')
    return render(request, 'story5/add.html', response)

def detail(request, my_id):
    obj = models.Matkul.objects.get(id = my_id)
    response = {
        "obj" : obj
    }

    return render(request, 'story5/detail.html', response)

def delete_matkul(request, my_id):
    obj = models.Matkul.objects.get(id = my_id)
    obj.delete()
    return HttpResponseRedirect('/story5/')