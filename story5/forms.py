from django import forms
from . import models

# from .models import Matkul
class Form_Matkul(forms.ModelForm):
    class Meta:
        model = models.Matkul
        fields = [
            'nama_matkul',
            'dosen',
            'sks',
            'tahun',
            'deskripsi',
            'ruang',
        ]

    error_message = {
        'required' : "Mohon masukan sesuatu"
    }

    nama_matkul = forms.CharField(
        max_length=30, widget=forms.TextInput(attrs = {
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'text',
        }))

    dosen = forms.CharField(max_length=30, widget=forms.TextInput(attrs = {
        'placeholder': 'Nama Dosen',
        'type' : 'text',
        }))

    sks = forms.CharField(max_length=30, widget=forms.TextInput(attrs = {
        'placeholder': 'SKS',
        'type' : 'number',
        'min' : "1",
        'max' : "24",
        }))

    tahun = forms.CharField(max_length=30, widget=forms.TextInput(attrs = {
        'placeholder': 'Gasal 2000/2001',
        'type' : 'text',
        }))

    deskripsi = forms.Textarea(attrs = {
        'placeholder': 'Deskripsi Mata Kuliah',
    })

    ruang = forms.CharField(max_length=30, widget=forms.TextInput(attrs = {
        'placeholder': 'Ruagan',
        'type' : 'text',
        }))



