from django.shortcuts import render

# Create your views here.
def story4(request):
    return render(request, 'story4/home.html')

def poster(request):
    return render(request, 'story4/poster.html')

def website(request):
    return render(request, 'story4/website.html')