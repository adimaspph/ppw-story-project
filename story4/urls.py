from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.story4, name='home'),
    path("poster", views.poster, name='poster'),
    path("website", views.website, name='website')
]