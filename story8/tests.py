from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import *
from .urls import *

class Story8TestCase(TestCase):
    def test_is_home_page_exists(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/home.html')

    def test_views_home(self):
        found = resolve('/story8/')            
        self.assertEqual(found.func, story8)

    def test_api_exist(self):
        response = Client().get('/story8/Genshin%20Impact/')
        self.assertEqual(response.status_code,200)

    def test_api_views_exist(self):
        found = resolve('/story8/Genshin%20Impact/')
        self.assertEqual(found.func, search_book)

    def test_home_create_url_exists(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)