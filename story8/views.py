from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http.response import JsonResponse
import urllib.request, json


# Create your views here.
def story8(request):

    return render(request, 'story8/home.html')


def search_book(request, title):
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q=" + title + "&maxResults=20", "/:?=&")
    with urllib.request.urlopen(link) as url:
        global data
        data = json.loads(url.read().decode())

    return JsonResponse(data)