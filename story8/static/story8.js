search("p")

$('#searchbar').keyup(function(){
    console.log($('#searchbar').val());
    var value = $('#searchbar').val();

    search(value)
})

function search(value) {
    $.ajax({
        url: "/story8/" + value,
        contentType: "application/json",
        dataType: 'json',
        success: function(response){
            let resultBox = $("#resultBox")
            let inner = ""
            let json = response.items
            let title = "";
            let image = "";
            let author = "";
            let result = "";
            for(let i=0;i<json.length;i++){
                title = json[i].volumeInfo.title;
                author = json[i].volumeInfo.authors;
                image = json[i].volumeInfo.imageLinks.thumbnail;

                result = '\
                <div class="resultCard"\>\
                <img src="' + image + '" alt=""\>\
                <h4>' + title + '</h4\>\
                <p>' + author + '</p\>\
                </div>'

                inner += result
                console.log(json[i]);
            }
            resultBox.html(inner)
        }
    })
}

//Oleh Adimas
