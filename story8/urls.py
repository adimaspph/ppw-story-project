from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8, name='home'),
    path('<str:title>/', views.search_book, name='cari')
]