from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    username = forms.CharField(label="Username", max_length=150, widget=forms.TextInput(attrs=
    {
        'class' : 'form-control',
        'placeholder' : 'Username',
    }))

    email = forms.EmailField(label="email", max_length=150, widget=forms.TextInput(attrs=
    {
        'class' : 'form-control',
        'placeholder' : 'Email',
    }))

    password1 = forms.CharField(label="Password", max_length=150, widget=forms.PasswordInput(attrs=
    {
        'class' : 'form-control',
        'placeholder' : 'Password',
    }))

    password2 = forms.CharField(label="Confirm Password", max_length=150, widget=forms.PasswordInput(attrs=
    {
        'class' : 'form-control',
        'placeholder' : 'Confirm Password',
    }))
