from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='home'),
    path('register/', views.registerUser, name='register'),
    path('login/', views.loginUser, name='login'),
    path('logout/', views.logoutUser, name='logout'),
]
