from django.test import TestCase, Client
from django.contrib.auth.models import User

class TestStory9(TestCase):
    def test_home_url_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_exist(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_add_user(self):
        user_count = User.objects.all().count()
        user = User.objects.create_user('test', 'test@mail.com', 'testtesttest')
        user.save()
        self.assertEqual(User.objects.all().count(), user_count + 1)

    def test_register_user_success(self):
        user_count = User.objects.all().count()
        user_credentials = {
            'username':'username',
            'email':'email@mail.com',
            'password1':'testUsernamePassword',
            'password2':'testUsernamePassword',
        }
        response = Client().post('/story9/register/', data = user_credentials, follow=True)
        content = response.content.decode('utf-8')
        self.assertEqual(User.objects.all().count(), user_count + 1)
        self.assertIn('Account was succesfully created', content)
        self.assertEqual(response.status_code, 200)

    def test_register_user_fail(self):
        user_count = User.objects.all().count()
        user_credentials = {
            'username':'username',
            'email':'email@mail.com',
            'password1':'testUsernamePasswordd',
            'password2':'testUsernamePassword',
        }
        response = Client().post('/story9/register/', data = user_credentials)
        content = response.content.decode('utf-8')
        self.assertEqual(User.objects.all().count(), user_count)
        self.assertIn('Account creation was unsuccessful', content)
        self.assertEqual(response.status_code, 200)

    # def test_login_user_success(self):
    #     user_credentials = {
    #         'username':'testUser',
    #         'password':'testUserPassword'
    #     }
    #     response = Client().post('/story9/login/', data = user_credentials, follow=True)
    #     content = response.content.decode('utf-8')
    #     self.assertIn(f'Welcome, {user_credentials["username"]}', content)
    #     self.assertEqual(response.status_code, 200)

    def test_login_user_fail(self):
        user_credentials = {
            'username':'testUserr',
            'password':'testUserPassword'
        }
        response = Client().post('/story9/login/', data = user_credentials)
        content = response.content.decode('utf-8')
        self.assertIn('Username or Password is incorrect', content)
        self.assertEqual(response.status_code, 200)

    def test_logout_user(self):
        Client().login(username='testUser', password='testUserPassword')
        response = Client().get('/story9/logout/', follow=True)
        content = response.content.decode('utf-8')
        self.assertIn('Welcome, Anonymous', content)
        self.assertEqual(response.status_code, 200)
