from django.db import models

from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Kegiatan(models.Model):
    kegiatan = models.CharField(max_length = 100)

    def __str__(self):
        return "{}".format(self.kegiatan)

    
class Anggota(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='peserta')
    anggota = models.CharField(max_length=100)

    def __str__(self):
        return "{:s}".format(self.anggota)
