from django.contrib import admin
from .models import Kegiatan, Anggota

# Register your models here.
admin.site.register(Anggota)
admin.site.register(Kegiatan)