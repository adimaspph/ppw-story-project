from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import TambahKegiatanForm, TambahPesertaForm
from .models import Kegiatan, Anggota

# Create your views here.
def story6(request):
    # form = TambahPesertaForm(request.POST or None)
    response = {
        "kegiatans" : Kegiatan.objects.all(),
        "form_peserta" : TambahPesertaForm,
    }

    return render(request, 'story6/home.html', response)

def tambah_peserta(request, id=None):
    form = TambahPesertaForm(request.POST or None)
    kegiatan = Kegiatan.objects.get(id=id)

    if (request.method == 'POST'):
        nama_anggota = form["anggota"].value()
        anggota = Anggota(kegiatan=kegiatan, anggota=nama_anggota)
        anggota.save()
    
    return HttpResponseRedirect('/story6/')

def tambah_kegiatan(request):
    form = TambahKegiatanForm(request.POST or None)
    response = {
        "form_kegiatan" : TambahKegiatanForm,
    }

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/story6/')

    return render(request, 'story6/tambahKegiatan.html', response)

