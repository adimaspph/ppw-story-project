from django.forms import ModelForm
from .models import Anggota, Kegiatan

class TambahPesertaForm(ModelForm):
    class Meta:
        model = Anggota
        fields = '__all__'


class TambahKegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
