from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import Anggota, Kegiatan
from .views import *
from .urls import *


class Story6TestCase(TestCase):

# Test untuk Views
    # Test Function
    def test_views_home(self):
        found = resolve('/story6/')            
        self.assertEqual(found.func, story6)

    def test_views_tambah_kegiatan(self):
        found = resolve('/story6/tambahkegiatan/')            
        self.assertEqual(found.func, tambah_kegiatan)

    def test_views_tambah_peserta(self):
        Kegiatan.objects.create(kegiatan='PPW')
        found = resolve('/story6/tambah-peserta/1')            
        self.assertEqual(found.func, tambah_peserta)
    
    # Test template used
    def test_is_home_page_exists(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/home.html')

    def test_is_tambah_kegiatan_page_exists(self):
        response = Client().get('/story6/tambahkegiatan/')
        self.assertTemplateUsed(response, 'story6/tambahKegiatan.html')


# Models Test
    
    # Module Functionality Test
    def test_check_model_create_kegiatan(self):
        Kegiatan.objects.create(kegiatan="PPW")
        amount = Kegiatan.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_create_anggota(self):
        Kegiatan.objects.create(kegiatan="PPW")
        obj1 = Kegiatan.objects.get(id=1)
        
        Anggota.objects.create(kegiatan=obj1, anggota="Adimas")
        amount = Anggota.objects.all().count()
        self.assertEqual(amount, 1)

    # Module Return Test
    def test_check_model_return_kegiatan(self):
        Kegiatan.objects.create(kegiatan="PPW")
        result = Kegiatan.objects.get(id=1)
        self.assertEqual(str(result), "PPW")

    def test_check_model_return_anggota(self):
        Kegiatan.objects.create(kegiatan="PPW")
        obj1 = Kegiatan.objects.get(id=1)
        
        Anggota.objects.create(kegiatan=obj1, anggota="Adimas")
        result = Anggota.objects.get(id=1)
        self.assertEqual(str(result), "Adimas")


#  Url Test
    
    #Get
    def test_home_create_url_exists(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_kegiatan_create_url_exists(self):
        response = Client().get('/story6/tambahkegiatan/')
        self.assertEqual(response.status_code, 200)

    #Post
    def test_tambah_kegiatan_post_url(self):
        response = Client().post('/story6/tambahkegiatan/', data={'kegiatan': 'Belajar',})
        amount = Kegiatan.objects.filter(kegiatan="Belajar").count()
        self.assertEqual(amount, 1)

    def test_tambah_peserta_post_url(self):
        Kegiatan.objects.create(kegiatan='PPW')
        response = Client().post('/story6/tambah-peserta/1', data={'anggota': 'Adimas',})
        amount = Anggota.objects.filter(anggota="Adimas").count()
        self.assertEqual(amount, 1)


