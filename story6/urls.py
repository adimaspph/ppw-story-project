from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.story6, name='home'),
    path('tambahkegiatan/', views.tambah_kegiatan, name='kegiatan'),
    path('tambah-peserta/<int:id>', views.tambah_peserta, name='add'),
    
]