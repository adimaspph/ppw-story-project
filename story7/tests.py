from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import *
from .urls import *

class Story7TestCase(TestCase):
    def test_is_home_page_exists(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/home.html')

    def test_views_home(self):
        found = resolve('/story7/')            
        self.assertEqual(found.func, story7)

    def test_home_create_url_exists(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)